import crypto.Key;
import crypto.implementation.*;
import helpers.*;

import java.io.*;
import java.util.*;


public class Main {

    public static void main(String[] args){

        algebraicke_struktury();
        //Permutations p = new Permutations();

        //testing_cipher();

        //testing_text();

        //encrypt_file();
        //testing_matrix();

        //testing_cryptoSystems();

        //zadanie();
        //Mmath.cartesianProductTest();
        //testing_hillCracker();
        //combi();
        //break_key();
        //testing_bigramfintess();
        //substi_trnaspo();

        //xzakiariasd();

    }

    private static void skuska(){
        String[] elems = {"b", "l", "o", "b", "n", "e", "a", "a", "k"};
        Mmath.combinationReplacements(elems, 4);
    }

    private static void testing_cipher(){

        System.out.println("\n\n-----Ceasar-Cipher------");
        CaesarKey cK = new CaesarKey(10);
        CaesarCipher cC = new CaesarCipher();
        String text = "aaabbbyyyxxxzzz";
        String encrypted_text = cC.encryption(cK, text);
        String decrypted_text = cC.decryption(cK, encrypted_text);
        System.out.println("text: " + text);
        System.out.println("encrypted_text: " + encrypted_text);
        System.out.println("decrypted_text: " + decrypted_text);


        System.out.println("\n\n-------Substitution-Cipher-----------");
        Character[] char_key = new Character[] {    'q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p',
                                                    'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l',
                                                    'z', 'x', 'c', 'v', 'b', 'n', 'm'};
        SubstitutionKey sK = new SubstitutionKey(char_key);
        SubstitutionCipher sC = new SubstitutionCipher();
        System.out.println("encryption key: " + sK.key2String(sK.getKey()));
        System.out.println("decryption key: " + sK.key2String(sK.getKey_inverse()));
        text = "aaabbbyyyxxxzzz";
        encrypted_text = sC.encryption(sK, text);
        decrypted_text = sC.decryption(sK, encrypted_text);
        System.out.println("text: " + text);
        System.out.println("encrypted_text: " + encrypted_text);
        System.out.println("decrypted_text: " + decrypted_text);

        System.out.println("\n\n---------Columnar-Transposition-Cipher------------");
        Integer[] columnar_key = {3, 2, 6, 4, 1, 5};
        ColumnarTranspositionKey ctK = new ColumnarTranspositionKey(columnar_key);
        ColumnarTranspositionCipher ctC = new ColumnarTranspositionCipher();
        text = "defendtheeastwallofthecastle";
        encrypted_text = ctC.encryption(ctK, text);
        decrypted_text = ctC.decryption(ctK, encrypted_text);
        System.out.println("text: " + text);
        System.out.println("encrypted text: " + encrypted_text);
        System.out.println("decrypted text: " + decrypted_text);

        System.out.println("\n\n--------Vigener-Cipher--------------------------");
        String key_word = "heslo";
        VigenerCipher vC = new VigenerCipher();
        VigenerKey vK = new VigenerKey(key_word);
        text = "testujemsifru";
        encrypted_text = vC.encryption(vK, text);
        decrypted_text = vC.decryption(vK, encrypted_text);
        System.out.println("text: " + text + " heslo: " + vK.getKey());
        System.out.println("encrypted text: " + encrypted_text);
        System.out.println("decrypted text: " + decrypted_text);

        System.out.println("\n\n----------Hill-Cipher------------------------");
        text = "hatshatshatsh";
        Integer[][] m1 = {
                {3, 3},
                {2, 5}
        };
        Integer[][] m2 = {
                {3, 10, 20},
                {20, 9, 17},
                {9, 4, 17}
        };
        HillCipher hC = new HillCipher();
        HillKey hK = new HillKey(new Matrix(m1));
        System.out.println("encryption key: " + hK.getKey());
        System.out.println("decryption key: " + hK.getInverse_key());
        encrypted_text = hC.encryption(hK, text);
        decrypted_text = hC.decryption(hK, encrypted_text);

        System.out.println("text: " + text);
        System.out.println("encrypted text: " + encrypted_text);
        System.out.println("decrypted text: " + decrypted_text + "\n-------------\n\n");

        hC = new HillCipher();
        hK = new HillKey(new Matrix(m2));
        text = "attackistonight";
        System.out.println("encryption key: " + hK.getKey());
        System.out.println("decryption key: " + hK.getInverse_key());
        encrypted_text = hC.encryption(hK, text);
        decrypted_text = hC.decryption(hK, encrypted_text);

        System.out.println("text: " + text);
        System.out.println("encrypted text: " + encrypted_text);
        System.out.println("decrypted text: " + decrypted_text);

        System.out.println("\n\n----------Hill-Cipher-Substitution-----------------------");
        text = "hatshatshatsh";

        hC = new HillCipher();
        hK = new HillKey(new Matrix(m1));
        sK = new SubstitutionKey(char_key);
        System.out.println("encryption key: " + hK.getKey());
        System.out.println("decryption key: " + hK.getInverse_key());
        System.out.println("encryption key: " + sK.key2String(sK.getKey()));
        System.out.println("decryption key: " + sK.key2String(sK.getKey_inverse()));
        encrypted_text = hC.encryptionSubstitution(hK, sK, text);
        decrypted_text = hC.decryptionSubstitution(hK, sK, encrypted_text);

        System.out.println("text: " + text);
        System.out.println("encrypted text: " + encrypted_text);
        System.out.println("decrypted text: " + decrypted_text + "\n-------------\n\n");

        hC = new HillCipher();
        hK = new HillKey(new Matrix(m2));
        text = "attackistonight";
        System.out.println("encryption key: " + hK.getKey());
        System.out.println("decryption key: " + hK.getInverse_key());
        System.out.println("encryption key: " + sK.key2String(sK.getKey()));
        System.out.println("decryption key: " + sK.key2String(sK.getKey_inverse()));
        encrypted_text = hC.encryptionSubstitution(hK, sK, text);
        decrypted_text = hC.decryptionSubstitution(hK, sK, encrypted_text);

        System.out.println("text: " + text);
        System.out.println("encrypted text: " + encrypted_text);
        System.out.println("decrypted text: " + decrypted_text);



    }

    private static void testing_text(){
        String in = "Tĥïŝ ĩš â fůňķŷ Šťŕĭńġ 46!!??Doctor Rossi";
        System.out.println(Text.convertToTSA(in, false));

        String prilohaA = Text.readText(new File("/home/vilo/IdeaProjects/ksif_uvod/prilohy/prilohaA.txt"));
        String prilohaB = Text.readText(new File("/home/vilo/IdeaProjects/ksif_uvod/prilohy/prilohaB.txt"));
        String prilohaC = Text.readText(new File("/home/vilo/IdeaProjects/ksif_uvod/prilohy/prilohaC.txt"));
        String trashText = "qwertyuiopasdfghjklzxcvbnmqwertyuiopasdfghjklzxcvbnm";
        String randomText = TextStatistics.entropy(2000);
        System.out.println(prilohaA);
        prilohaA = Text.convertToTSA(prilohaA, false);
        prilohaB = Text.convertToTSA(prilohaB, false);
        prilohaC = Text.convertToTSA(prilohaC, false);
        randomText = Text.convertToTSA(randomText, false);
        Map<String, Double> map_ngram = TextStatistics.readNgram(randomText, 1, true);
        System.out.println(map_ngram.toString());
        Text.saveToFile(map_ngram, "/home/vilo/IdeaProjects/ksif_uvod/prilohy/prilohaA_ngram3rel");
        Map<String, Double> map_ngram2 = (Map<String, Double>)Text.readFromFile("/home/vilo/IdeaProjects/ksif_uvod/prilohy/prilohaA_ngram3rel");
        System.out.println("mapngram2: \n" + map_ngram2.toString());


        map_ngram = TextStatistics.readNgram(prilohaA, 1, true);
        Double[] relativeOccurenceA = new Double[map_ngram.values().size()];
        map_ngram.values().toArray(relativeOccurenceA);
        Double ixc_relativeA = TextStatistics.indexOfCoincidence(relativeOccurenceA);

        map_ngram = TextStatistics.readNgram(prilohaA, 1, false);
        Double[] absoluteOccurenceA = new Double[map_ngram.values().size()];
        map_ngram.values().toArray(absoluteOccurenceA);
        Double ixc_absoluteA = TextStatistics.indexOfCoincidence(absoluteOccurenceA, prilohaA.length());

        System.out.println("relative index of coincidence of text A: " + ixc_relativeA);
        System.out.println("absolute index of coincidence of text A: " + ixc_absoluteA);

        Language.LanguageEnum langA = Language.guessLanguage(prilohaA);
        Language.LanguageEnum langB = Language.guessLanguage(prilohaB);
        Language.LanguageEnum langC = Language.guessLanguage(prilohaC);
        Language.LanguageEnum langTrash = Language.guessLanguage(trashText);
        System.out.println("prilohaA guess language: " + langA);
        System.out.println("prilohaB guess language: " + langB);
        System.out.println("prilohaC guess language: " + langC);
        System.out.println("trash text guess language: " + langTrash);


        String prilohaA_encrypt = Text.readText(new File("/home/vilo/IdeaProjects/ksif_uvod/prilohy/prilohaA_encrypt.txt"));
        Map<String, Double> map_ngram3 = TextStatistics.readNgram(prilohaA_encrypt, 1, true);
        Map<String, Double> map_ngram4 = TextStatistics.readNgram(prilohaA_encrypt, 2, true);

        map_ngram3.values().toArray(relativeOccurenceA);
        Double ixc_relativeA_encrypt = TextStatistics.indexOfCoincidence(relativeOccurenceA);
        map_ngram4.values().toArray(relativeOccurenceA);
        Double ixc_relativeA_encrypt2 = TextStatistics.indexOfCoincidence(relativeOccurenceA);

        System.out.println("");
        System.out.println("mapngram3: \n" + map_ngram3.toString());
        System.out.println("mapngram4: \n" + map_ngram4.toString());
        System.out.println("relative index of coincidence of text A encrypt 1gram: " + ixc_relativeA_encrypt);
        System.out.println("relative index of coincidence of text A encrypt 2gram: " + ixc_relativeA_encrypt2);


    }

    private static void testing_cryptoSystems(){

        System.out.println("-------Testing-Crypto-Systems------------");
        CryptoSystems cs = new CryptoSystems();
        String prilohaA = Text.readText(new File("/home/vilo/IdeaProjects/ksif_uvod/prilohy/prilohaA.txt")).trim();
        String prilohaA_Vigener = Text.readText(new File("/home/vilo/IdeaProjects/ksif_uvod/prilohy/prilohaA_Vigener.txt")).trim();
        String prilohaA_Columnar = Text.readText(new File("/home/vilo/IdeaProjects/ksif_uvod/prilohy/prilohaA_Columnar.txt")).trim();
        String prilohaA_Substitution = Text.readText(new File("/home/vilo/IdeaProjects/ksif_uvod/prilohy/prilohaA_Substitution.txt")).trim();
        prilohaA  = Text.convertToTSA(prilohaA, false);

        System.out.println("-------Columnar-Transposition------------");
        Map<String, Double> map_1gram = TextStatistics.readNgram(prilohaA_Columnar, 1, true);
        Map<String, Double> map_2gram = TextStatistics.readNgram(prilohaA_Columnar, 2, true);
        Double diff_1 = cs.compareOccurrence(map_1gram, Language.refMapEnglish_monograms);
        Double diff_2 = cs.compareOccurrence(map_2gram, Language.refMapEnglish_bigrams);

        System.out.println("Sumation difference 1: " + diff_1);
        System.out.println("Sumation difference 2: " + diff_2);
        CryptoSystems.guess(prilohaA_Columnar);


        System.out.println("-------Substitution------------");
        map_1gram = TextStatistics.readNgram(prilohaA_Substitution, 1, true);
        map_2gram = TextStatistics.readNgram(prilohaA_Substitution, 2, true);
        diff_1 = cs.compareOccurrence(map_1gram, Language.refMapEnglish_monograms);
        diff_2 = cs.compareOccurrence(map_2gram, Language.refMapEnglish_bigrams);

        System.out.println("Sumation difference 1: " + diff_1);
        System.out.println("Sumation difference 2: " + diff_2);
        CryptoSystems.guess(prilohaA_Substitution);

        System.out.println("-------Vigener------------");
        CryptoSystems.guess(prilohaA_Vigener);

        System.out.println("-------testing slicing string");
        System.out.println(Text.stringSlice("abcdefghijklmnopqrstuvwxyz", 0, 26, 3));



    }

    private static void break_key(){
        CryptoSystems cs = new CryptoSystems();
        String prilohaA_Vigener = Text.readText(new File("/home/vilo/IdeaProjects/ksif_uvod/prilohy/prilohaA_Vigener.txt")).trim();

        System.out.println("-----------testing-guessKeylength");
        Integer key_prilohaA = cs.guessKeyLength(prilohaA_Vigener, Language.LanguageEnum.English);

        System.out.println("keylen prilohaA_Vigener: " + key_prilohaA);

        System.out.println("------------guess-key----------");
        cs.guessKey(prilohaA_Vigener, Language.LanguageEnum.English);
    }

    private void generate_columnar(){
        System.out.println("\n\n---------Columnar-Transposition-Cipher------------");
        String keyword = "juanmartindelpotrorafaelnadal";
        Integer[] columnar_key = Key.word2perm(keyword);
        Permutations.print_ObjArr(columnar_key);
        ColumnarTranspositionKey ctK = new ColumnarTranspositionKey(columnar_key);
        ColumnarTranspositionCipher ctC = new ColumnarTranspositionCipher();
        String text = "Polícia v meste Pittsburgh v americkom štáte Pennsylvánia zasahuje v prípade streľby, ku ktorej došlo v tamojšej synagóge. Incident si podľa prvých správ vyžiadal sedem mŕtvych, dvaja policajti utrpeli strelné zranenia.";
        text = Text.convertToTSA(text, false);
        String encrypted_text = ctC.encryption(ctK, text);
        String decrypted_text = ctC.decryption(ctK, encrypted_text);
        System.out.println("text: " + text);
        System.out.println("encrypted text: " + encrypted_text);
        System.out.println("decrypted text: " + decrypted_text);
    }

    private static void testing_matrix(){
        Integer[][] matrix1 = {
                {1, 2, 3, 22},
                {4, 3, 6, 14},
                {7, 8, 9, -6},
                {10, 11, 12, 2}
        };
        Integer[][] matrix2 = {
                {2, 11, 9},
                {8, -2, 1},
                {0, 3, 6},
                {1, 2, 3}
        };
        Integer[][] matrix3 = {
                {3, 3},
                {2, 5},
        };
        Matrix m1 = new Matrix(matrix1);
        Matrix m2 = new Matrix(matrix2);
        Matrix m3 = new Matrix(matrix3);
        System.out.println(m1);
        System.out.println(m1.determinant());
        m1 = m1.multiplyConstant(-2);
        System.out.println(m1);
        m1 = m1.modulo(3);
        System.out.println(m1);

        System.out.println("-------------testing-inverse-number-----------");
        System.out.println(Mmath.inverseNumberMod(1,2));
        System.out.println(Mmath.inverseNumberMod(7,20));
        System.out.println(Mmath.inverseNumberMod(2,4));

        System.out.println("-------------testing-muliplication-matrices-------");
        m1.setMatrix(matrix1);
        System.out.println(m1.multitplyMatrix(m2));

        System.out.println("-------------testing-submatrix-------");
        Matrix sub = new Matrix(m1.submatrix(1,2));
        System.out.println(m1);
        System.out.println(sub);

        System.out.println(m3);
        sub = new Matrix(m3.submatrix(0,0));
        System.out.println(sub);

        System.out.println("-------------testing-cofactor--------");
        m1.setMatrix(matrix1);
        System.out.println(m1);
        System.out.println(m1.cofactor());

        System.out.println("-------------testing-transpose--------");
        System.out.println(m1.transpose());

        System.out.println("-------------testing-inverseMod--------");
        Matrix m3i = m3.inverseMod(26);
        System.out.println(m3);
        System.out.println(m3.inverseMod(26));
        System.out.println(m3.multitplyMatrix(m3i));
    }

    private static void encrypt_file(){
        // temp function for encrypting attachments
        String prilohaA = Text.readText(new File("/home/vilo/IdeaProjects/ksif_uvod/prilohy/prilohaA.txt"));
        String openText = Text.convertToTSA(prilohaA, false);

        // vigener cipher
        String key_word = "heslo";
        VigenerCipher vC = new VigenerCipher();
        VigenerKey vK = new VigenerKey(key_word);
        String encrypted_text = vC.encryption(vK, openText);
        Text.writeText( new File("/home/vilo/IdeaProjects/ksif_uvod/prilohy/prilohaA_Vigener.txt"), encrypted_text);

        // columnar transposition
        Integer[] columnar_key = {3, 2, 6, 4, 1, 5};
        ColumnarTranspositionCipher cC = new ColumnarTranspositionCipher();
        ColumnarTranspositionKey cK = new ColumnarTranspositionKey(columnar_key);
        encrypted_text = cC.encryption(cK, openText);
        Text.writeText( new File("/home/vilo/IdeaProjects/ksif_uvod/prilohy/prilohaA_Columnar.txt"), encrypted_text);

        // substitution
        Character[] char_key = new Character[] {    'q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p',
                'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l',
                'z', 'x', 'c', 'v', 'b', 'n', 'm'};
        SubstitutionKey sK = new SubstitutionKey(char_key);
        SubstitutionCipher sC = new SubstitutionCipher();
        encrypted_text = sC.encryption(sK, openText);
        Text.writeText(new File("/home/vilo/IdeaProjects/ksif_uvod/prilohy/prilohaA_Substitution.txt"), encrypted_text);
    }

    private static void testing_bigramfintess(){
        System.out.println("------------testing-bigramfitness");
        //Language.print_ref(Language.ref_bi);

        String prilohaA_Columnar = Text.readText(new File("/home/vilo/IdeaProjects/ksif_uvod/prilohy/prilohaA_Columnar.txt")).trim();
        String prilohaA = Text.readText(new File("/home/vilo/IdeaProjects/ksif_uvod/prilohy/prilohaA.txt")).trim();
        String prilohaA_Vigener = Text.readText(new File("/home/vilo/IdeaProjects/ksif_uvod/prilohy/prilohaA_Vigener.txt")).trim();


        System.out.println("---------------Dictionary---------");
        List<String> words =  Node.readDictionaryWords("./prilohy/dictionary_5000.txt");
        Node node = Node.loadDictionary(words);
        System.out.println(node);
        double r1 = node.evaluate("hellworldwide", 4, 11);
        System.out.println("r1: " + r1);
    }

    private static void zadanie(){
        String prilohaC = Text.readText(new File("/home/vilo/IdeaProjects/ksif_uvod/prilohy/prilohaC.txt"));
        String text = Text.convertToTSA(prilohaC, false);
        HillCipher hC = new HillCipher();
        Integer[][] m2 = {
                {3, 10, 20},
                {20, 9, 17},
                {9, 4, 17}
        };
        HillKey hK = new HillKey(new Matrix(m2));
        Character[] char_key = new Character[] {    'q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p',
                'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l',
                'z', 'x', 'c', 'v', 'b', 'n', 'm'};
        SubstitutionKey sK = new SubstitutionKey(char_key);

        System.out.println("text: " + text.substring(0, 100));
        System.out.println("pocet znakov: " + text.length());

        String encrypted_text = hC.encryption(hK, text);
        System.out.println("encrypted text: " + encrypted_text.substring(0, 100));

        String encrypted_textSubsti = hC.encryptionSubstitution(hK, sK, text);
        System.out.println("encrypted text Substi: " + encrypted_textSubsti.substring(0, 100));

        String decrypted_text = hC.decryption(hK, encrypted_text);
        System.out.println("decrypted text: " + decrypted_text.substring(0, 100));

        String decrypted_textSubsti = hC.decryptionSubstitution(hK, sK, encrypted_textSubsti);
        System.out.println("decrypted text Substi: " + decrypted_textSubsti.substring(0, 100));

        Map<String, Double> map_ngram_OT = TextStatistics.readNgram(text, 1, true);
        Double[] relativeOccurence_OT = new Double[map_ngram_OT.values().size()];
        map_ngram_OT.values().toArray(relativeOccurence_OT);
        Double ic_OT = TextStatistics.indexOfCoincidence(relativeOccurence_OT);

        Map<String, Double> map_ngram_ZT = TextStatistics.readNgram(encrypted_text, 1, true);
        Double[] relativeOccurence_ZT = new Double[map_ngram_ZT.values().size()];
        map_ngram_ZT.values().toArray(relativeOccurence_ZT);
        Double ic_ZT = TextStatistics.indexOfCoincidence(relativeOccurence_ZT);

        Map<String, Double> map_ngram_sZT = TextStatistics.readNgram(encrypted_textSubsti, 1, true);
        Double[] relativeOccurence_sZT = new Double[map_ngram_sZT.values().size()];
        map_ngram_sZT.values().toArray(relativeOccurence_sZT);
        Double ic_sZT = TextStatistics.indexOfCoincidence(relativeOccurence_sZT);

        System.out.println("ic_OT: " + ic_OT);
        System.out.println("ic_ZT: " + ic_ZT);
        System.out.println("ic_sZT: " + ic_sZT);

        String _3k_0_OT = "";
        String _3k_0_ZT = "";
        String _3k_1_OT = "";
        String _3k_1_ZT = "";
        String _3k_2_OT = "";
        String _3k_2_ZT = "";
        for(int k=0; k<text.length()/3; k++){
            _3k_0_OT += text.charAt(3*k);
            _3k_0_ZT += encrypted_text.charAt(3*k);
            _3k_1_OT += text.charAt(3*k + 1);
            _3k_1_ZT += encrypted_text.charAt(3*k + 1);
            _3k_2_OT += text.charAt(3*k + 2);
            _3k_2_ZT += encrypted_text.charAt(3*k + 2);
        }

        Map<String, Double> map_ngram = TextStatistics.readNgram(_3k_0_OT, 1, true);
        Double[] relativeOccurence = new Double[map_ngram.values().size()];
        map_ngram.values().toArray(relativeOccurence);
        Double ic = TextStatistics.indexOfCoincidence(relativeOccurence);
        System.out.println("_3k_0_OT IC: " + ic);

        map_ngram = TextStatistics.readNgram(_3k_0_ZT, 1, true);
        relativeOccurence = new Double[map_ngram.values().size()];
        map_ngram.values().toArray(relativeOccurence);
        ic = TextStatistics.indexOfCoincidence(relativeOccurence);
        System.out.println("_3k_0_ZT IC: " + ic);

        map_ngram = TextStatistics.readNgram(_3k_1_OT, 1, true);
        relativeOccurence = new Double[map_ngram.values().size()];
        map_ngram.values().toArray(relativeOccurence);
        ic = TextStatistics.indexOfCoincidence(relativeOccurence);
        System.out.println("_3k_1_OT IC: " + ic);

        map_ngram = TextStatistics.readNgram(_3k_1_ZT, 1, true);
        relativeOccurence = new Double[map_ngram.values().size()];
        map_ngram.values().toArray(relativeOccurence);
        ic = TextStatistics.indexOfCoincidence(relativeOccurence);
        System.out.println("_3k_1_ZT IC: " + ic);

        map_ngram = TextStatistics.readNgram(_3k_2_OT, 1, true);
        relativeOccurence = new Double[map_ngram.values().size()];
        map_ngram.values().toArray(relativeOccurence);
        ic = TextStatistics.indexOfCoincidence(relativeOccurence);
        System.out.println("_3k_2_OT IC: " + ic);

        map_ngram = TextStatistics.readNgram(_3k_2_ZT, 1, true);
        relativeOccurence = new Double[map_ngram.values().size()];
        map_ngram.values().toArray(relativeOccurence);
        ic = TextStatistics.indexOfCoincidence(relativeOccurence);
        System.out.println("_3k_2_ZT IC: " + ic);

        //uloha5(encrypted_text, "/home/vilo/IdeaProjects/ksif_uvod/prilohy/icZ.txt");
        //uloha5(encrypted_textSubsti, "/home/vilo/IdeaProjects/ksif_uvod/prilohy/icsZ.txt");
        hC.keySolver("dalsivyzn", "vngijfkqr", 3);

    }

    private static void testing_hillCracker(){
        ArrayList<ArrayList<Matrix>> allKeys = new ArrayList<>();
        HillCipher hC = new HillCipher();
        ArrayList<Matrix> keys2 = hC.keySolver("ackistoni", "agwjgjkdn", 3);
        ArrayList<Matrix> keys1 = hC.keySolver("attackist", "fnwagwjgj", 3);
        allKeys.add(keys1); allKeys.add(keys2);
        Matrix key = hC.keyIntersection(allKeys);
        System.out.println(key);
    }

    public static void combi()
    {
        ArrayList<ArrayList<Integer>> combos = Mmath.combinationsReplacement(4, 3);
        for(ArrayList<Integer> ar: combos){
            System.out.println(ar);
        }
    }

    private static void uloha5(String encrypted_text, String fileName){

        for(int a0=0; a0<26; a0++){
            for(int a1=0; a1<26; a1++){
                for(int a2=0; a2<26; a2++){
                    String z = "";
                    int zi;
                    assert encrypted_text.length() % 3 == 0;

                    for(int i=0; i<encrypted_text.length()/3; i++) {
                        zi =    a0 * (encrypted_text.charAt(3*i) - 'a') +
                                a1 * (encrypted_text.charAt(3*i + 1) - 'a') +
                                a2 * (encrypted_text.charAt(3*i + 2) - 'a');
                        zi %= 26;
                        z += (char)(zi + 'a');
                    }
                    Map<String, Double> map_ngram = TextStatistics.readNgram(z, 1, true);
                    Double[] relativeOccurence = new Double[map_ngram.values().size()];
                    map_ngram.values().toArray(relativeOccurence);
                    Double ic = TextStatistics.indexOfCoincidence(relativeOccurence);
                    String output = String.format("(%d, %d, %d) -> %f\n", a0, a1, a2, ic);
                    try(FileWriter fw = new FileWriter(fileName, true);
                        BufferedWriter bw = new BufferedWriter(fw);
                        PrintWriter out = new PrintWriter(bw))
                    {
                        out.print(output);
                    } catch (IOException e) {

                    }
                }
            }
        }
    }

    private static void substi_trnaspo(){
        Character[] char_key = new Character[] {    'q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p',
                'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l',
                'z', 'x', 'c', 'v', 'b', 'n', 'm'};
        SubstitutionKey sK = new SubstitutionKey(char_key);
        SubstitutionCipher sC = new SubstitutionCipher();

        Integer[] columnar_key = {3, 2, 6, 4, 1, 5};
        ColumnarTranspositionKey ctK = new ColumnarTranspositionKey(columnar_key);
        ColumnarTranspositionCipher ctC = new ColumnarTranspositionCipher();

        String text = "helloworldaa";
        String ST = sC.encryption(sK, ctC.encryption(ctK, text));
        String TS = ctC.encryption(ctK, sC.encryption(sK, text));

        System.out.println("S(T(x)): " + ST);
        System.out.println("T(S(x)): " + TS);
        System.out.println("ST == TS : " + ST.equals(TS));
    }

    private static void xzakiariasd(){

        String encrypted_text = Text.readText(new File("/home/vilo/IdeaProjects/ksif_uvod/prilohy/cipher_xbalazv.txt")).toLowerCase().trim();
        System.out.println("lent text: " + encrypted_text.length());
        //encrypted_text = encrypted_text.replaceAll("x", "");
        CryptoSystems cs = new CryptoSystems();
        System.out.println("encrypted text: \n" + encrypted_text);

        Map<String, Double> map_1gram = TextStatistics.readNgram(encrypted_text, 1, true);
        Map<String, Double> map_2gram = TextStatistics.readNgram(encrypted_text, 2, true);

        System.out.println("map_1gram : " + map_1gram.size() + ":\n" + map_1gram);

        Double diff_1 = cs.compareOccurrence(map_1gram, Language.refMapEnglish_monograms);
        //Double diff_2 = cs.compareOccurrence(map_2gram, Language.refMapEnglish_bigrams);

        System.out.println("Sumation difference 1: " + diff_1);
        //System.out.println("Sumation difference 2: " + diff_2);
        CryptoSystems.guess(encrypted_text);

        int len = cs.guessKeyLength(encrypted_text, Language.LanguageEnum.English);
        System.out.println("key length : " + len);

        System.out.println("-----------testing-guessKeylength");
        Integer key_prilohaA = cs.guessKeyLength(encrypted_text, Language.LanguageEnum.English);


    }

    public static void algebraicke_struktury(){
        Integer p[] = {4,3,5,1,2,6,8,7};
        Integer q[] = {1,3,4,2,5,7,6,8};
        Integer r[] = {3,4,5,2,1,8,7,6};
        Permutations.print_ObjArr(Permutations.compoundPermutation(Permutations.compoundPermutation(p,q), r));
        Permutations.print_ObjArr(Permutations.compoundPermutation(Permutations.compoundPermutation(p,r), q));
        Permutations.print_ObjArr(Permutations.compoundPermutation(Permutations.compoundPermutation(r,q), p));
        Permutations.print_ObjArr(Permutations.compoundPermutation(Permutations.compoundPermutation(r,p), q));
        Permutations.print_ObjArr(Permutations.compoundPermutation(Permutations.compoundPermutation(q,p), r));
        Permutations.print_ObjArr(Permutations.compoundPermutation(Permutations.compoundPermutation(q,r), p));
    }

}




