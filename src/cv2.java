import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Random;

public class cv2 {

    public void mmain(){
        HashSet<Integer> var10_26 = permutacia_10();
        System.out.println(var10_26);
        ArrayList<Integer> perm10 = new ArrayList<>(var10_26);

        for(int i=0; i<5; i++){
            Collections.shuffle(perm10);
            System.out.println(perm10);
        }

    }

    public HashSet<Integer> permutacia_10(){
        // vytvory permutaciu k = 10, nahodne usporiadanych
        HashSet<Integer> perm10 = new HashSet<>();
        while(perm10.size() != 10){
            Random x = new Random();
            int n = x.nextInt(10) + 1; // <1, 10>
            perm10.add(n);
        }
        return perm10;
    }
}
