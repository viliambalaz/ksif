import helpers.Mmath;

import java.util.*;

public class cv1 {

    public static void mmain(String[] args){
        System.out.println("hello world");

        for(int i=1; i<=21; i++){
            String pn = i%2==0 ? "parne": "neparne";
            System.out.println(i + "- " + pn);
        }

        int[] p = new int[12];
        for(int i=0; i<p.length; i++){
            p[i] = i+1;
        }

        for(int i: p){
            System.out.println(i);
        }


        int n;
        do{
            Random x = new Random();
            n = x.nextInt(99) + 1;
            System.out.println(n);
        }while(!Mmath.isPrime(n));

        ArrayList<Double> z = new ArrayList<Double>();
        for(int i=0; i<100; i++){
            Random x = new Random();
            z.add(x.nextDouble());
        }
        Collections.sort(z);
        Collections.reverse(z);
        for(double d: z){
            System.out.println(d);
        }

        String mena[] = new String[]{"Dolar","Libra","Rubel","Frank"};
        List list1 = Arrays.asList(mena);
        String mena2[] = new String[list1.size()];
        list1.toArray(mena2);
        System.out.println(list1.toString());

        /*
        File f = Text.pickFromFileChooser();
        String[] slova = Text.readText(f).split("\n");
        HashSet<String> hs = new HashSet<String>();
        for(String s: slova){
            s = s.toLowerCase();
            if(s.charAt(0) == 'm'){
                continue;
            }
            hs.add(s);
        }

        System.out.println("size: " + hs.size());
        */

        ArrayList<String> arr = new ArrayList<String>();
        n_reraz(arr, "", 3);
        //System.out.println("combinations_replacement: " + );

        for(String s: arr){
            System.out.println(s);
        }


    }


    public static void n_reraz(ArrayList<String> results, String word, int n){
        // vrati zoznam s retazcami o dlzke n

        if(word.length() == n){
            results.add(word);
            return;
        }

        String alphabet = "abcdefghijklmnopqrstuvwxyz";
        for(int i=0; i<alphabet.length(); i++){
            String w = word + alphabet.charAt(i);
            n_reraz(results, w, n);
        }
    }
}
