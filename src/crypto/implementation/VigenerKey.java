package crypto.implementation;

import crypto.Key;

public class VigenerKey implements Key {

    private String key; // key represents a simple word which repeats, each letter means shifting (like Ceaser)

    public VigenerKey(String key){
        this.key = key;
    }

    public String getKey() {
        return key;
    }
}
