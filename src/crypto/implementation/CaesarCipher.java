package crypto.implementation;

import crypto.Cipher;
import crypto.Key;

public class CaesarCipher implements Cipher {

    private String alphabet;
    private int no_char; // number of characters

    public CaesarCipher(){
        alphabet = Key.ALPHABET;
        no_char = alphabet.length();

    }

    @Override
    public String encryption(Key key, String text) {
        CaesarKey ck = (CaesarKey) key;
        char[] encrypted_text = new char[text.length()];
        for(int i=0; i<text.length(); i++){
            encrypted_text[i] = (char)(  ( ((int)text.charAt(i) - (int)'a' + ck.getKey()) % no_char) + (int)'a');
        }
        return new String(encrypted_text);
    }

    @Override
    public String decryption(Key key, String text) {
        CaesarKey ck = (CaesarKey) key ;
        char[] decrypted_text = new char[text.length()];
        for(int i=0; i<text.length(); i++){
            decrypted_text[i] = (char)(  ( ((int)text.charAt(i) - (int)'a' - ck.getKey() + no_char) % no_char) + (int)'a');
        }
        return new String(decrypted_text);
    }
}
