package crypto.implementation;

import crypto.Cipher;
import crypto.Key;

public class SubstitutionCipher implements Cipher{

    public SubstitutionCipher() {
    }

    @Override
    public String encryption(Key key, String text) {
        SubstitutionKey sK = (SubstitutionKey) key;
        char[] encrypted_text = new char[text.length()];
        for(int i=0; i<text.length(); i++){
            encrypted_text[i] = sK.getKey()[text.charAt(i)-'a']; // 'a' -> 0
        }
        return new String(encrypted_text);
    }

    @Override
    public String decryption(Key key, String text) {
        SubstitutionKey sK = (SubstitutionKey) key;
        Character[] inverseKey = sK.getKey_inverse();
        char[] decrypted_text = new char[text.length()];
        //System.out.println("key:         " + Cipher.repr(sK.getKey()));
        //System.out.println("inverse key: " + Cipher.repr(inverseKey));
        for(int i=0; i<text.length(); i++){
            decrypted_text[i] = inverseKey[text.charAt(i)-'a'];
        }
        return new String(decrypted_text);
    }
}
