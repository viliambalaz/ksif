package crypto.implementation;

import crypto.Key;

public class CaesarKey implements Key {

    private int key;

    public CaesarKey(int key){
        this.key = key;
    }

    public int getKey() {
        return key;
    }
}
