package crypto.implementation;

import helpers.Language;
import helpers.Text;
import helpers.TextStatistics;

import java.util.ArrayList;
import java.util.Map;

public class CryptoSystems {

    /**
     * function to determine the type of classic Cypher from text
     * @param txt encrypted TSA text
     * @return  int value for type of Cipher
     *      0 -
     *      1 -
     */
    public static int guess(String txt){
        Map<String, Double> map_ngram = TextStatistics.readNgram(txt, 1, true);
        Double[] relativeOccurrence = new Double[map_ngram.values().size()];
        map_ngram.values().toArray(relativeOccurrence);
        Double ic_absolute = TextStatistics.indexOfCoincidence(relativeOccurrence);

        Language.LanguageEnum lang = Language.guessLanguage(txt);
        System.out.println("Occurence" + map_ngram);
        System.out.println("ic: " + ic_absolute);
        System.out.println("lang: " + lang);
        return 0;
    }

    /**
     * test for Substitution Cipher
     * sort occurrence and ref and compare as above
     * @param occurrence
     * @param ref
     * @return sum(|M_i - R_i|) ~ 0;
     */
    public Double compareOccurrence(Map<String, Double> occurrence, Map<String, Double> ref){
        Double sum = 0.0;
        for(String c: occurrence.keySet()){
            sum += Math.abs(occurrence.get(c) - ref.get(c));
        }
        return sum;
    }

    public Integer guessKeyLength(String encrypted_text, Language.LanguageEnum lang){

        for(int i=2; i<20; i++){
            String sliced1 = Text.stringSlice(encrypted_text, 0, encrypted_text.length(), i);
            String sliced2 = Text.stringSlice(encrypted_text, 1, encrypted_text.length(), i);

            Map<String, Double> map_ngram1 = TextStatistics.readNgram(sliced1, 1, true);
            Double[] relativeOccurrence1 = new Double[map_ngram1.values().size()];
            map_ngram1.values().toArray(relativeOccurrence1);
            Double ic1 = TextStatistics.indexOfCoincidence(relativeOccurrence1);

            Map<String, Double> map_ngram2 = TextStatistics.readNgram(sliced2, 1, true);
            Double[] relativeOccurrence2 = new Double[map_ngram2.values().size()];
            map_ngram2.values().toArray(relativeOccurrence2);
            Double ic2 = TextStatistics.indexOfCoincidence(relativeOccurrence2);
            /*
            System.out.println(String.format("i: %d\tic1: %f lang1: %s\tic2: %f lang2: %s", i, ic1, Language.guessLanguage(sliced1), ic2, Language.guessLanguage(sliced2)));
            System.out.println("slice1: " + sliced1.substring(0, 100));
            System.out.println("slice2: " + sliced2.substring(0, 100));
            System.out.println();
            */
            if(Language.guessLanguage(sliced1) == lang && Language.guessLanguage(sliced2) == lang){
                return i;
            }


        }
        return 0;
    }

    public Integer guessKey(String encrypted_text, Language.LanguageEnum lang){
        Integer keylen = guessKeyLength(encrypted_text, lang);
        //ArrayList<ArrayList<Integer>> combi = Mmath.combinationsReplacement(26, 5); //.OutOfMemoryError
        Integer i=0;
        for(char a ='g'; a<='z'; a++){
        for(char b ='a'; b<='z'; b++){
        for(char c ='a'; c<='z'; c++){
        for(char d ='a'; d<='z'; d++){
        for(char e ='a'; e<='z'; e++){
            String key = "" + a + b + c + d + e;
            VigenerKey vK = new VigenerKey(key);
            VigenerCipher vC = new VigenerCipher();
            String decrypted_text = vC.decryption(vK, encrypted_text);
            Map<String, Double> map_1gram = TextStatistics.readNgram(decrypted_text, 1, true);
            Double diff = compareOccurrence(map_1gram, Language.refMapEnglish_monograms);
            Double dict = Language.EnglishDictionary.evaluate(decrypted_text, 3, 13);

            if(diff < 0.3){
                System.out.println(String.format("i: %d\t diff: %f\t dict: %f\t key: %s\t text: %s", i, diff, dict, key, decrypted_text.substring(0, 100)));
            }
            i++;
        } } } } }


        return 0;
    }

    public Integer guessKey2(String encrypted_text, Language.LanguageEnum lang){
        Integer keylen = guessKeyLength(encrypted_text, lang);
        //ArrayList<ArrayList<Integer>> combi = Mmath.combinationsReplacement(26, 5); //.OutOfMemoryError
        Integer i=0;
        Double min_diff = 1.0;
        VigenerKey bestKey = new VigenerKey("aaaaa");
        for(char a ='a'; a<='z'; a++){
        for(char b ='a'; b<='z'; b++){
        for(char c ='a'; c<='z'; c++){
        for(char d ='a'; d<='z'; d++){
        for(char e ='a'; e<='z'; e++){
            String key = "" + a + b + c + d + e;
            VigenerKey vK = new VigenerKey(key);
            VigenerCipher vC = new VigenerCipher();
            String decrypted_text = vC.decryption(vK, encrypted_text);
            Map<String, Double> map_2gram = TextStatistics.readNgram(decrypted_text, 2, true);
            Double diff = compareOccurrence(map_2gram, Language.refMapEnglish_bigrams);
            if(diff < 0.7){
                System.out.println(String.format("i: %d\t diff: %f\t key: %s \t text: %s", i, diff, key, decrypted_text.substring(0, 100)));
                if(diff < min_diff){
                    min_diff = diff;
                    bestKey = vK;
                }
            }
            i++;
        } } } } }
        System.out.println("min_diff: " + min_diff + "\tbest key: " + bestKey.getKey());
        return 0;
    }

    public VigenerKey guessKey(String encrypted_text, Language.LanguageEnum lang, int keyLength)
    {
        Double min_diff = 1.0;
        int  n = 26 - 1; // number of character in alphabet
        int nn = n + 1;
        int p = 1;
        VigenerCipher vC = new VigenerCipher();
        VigenerKey bestKey = new VigenerKey("aaaaa");
        for (int i=0; i<keyLength; i++)
            p *= nn;
        for (int i=0; i<p; i++){
            int t = i;
            String key = "";
            for (int j=0; j<keyLength; j++){
                key += (char)((t % nn) + 'a');
                t = t / nn;
            }
            // combi
            VigenerKey vK = new VigenerKey(key);
            String decrypted_text = vC.decryption(vK, encrypted_text);
            Map<String, Double> map_gram = TextStatistics.readNgram(decrypted_text, 1, true);
            Double diff = compareOccurrence(map_gram, Language.refMapEnglish_monograms);
            Double dict = Language.EnglishDictionary.evaluate(decrypted_text, 3, 13);
            if(diff < 0.3){
                System.out.println(String.format("i: %d\t diff: %f\t dictionary: %f\t key: %s \t text: %s", i, diff, dict, key, decrypted_text.substring(0, 100)));
                if(diff < min_diff){
                    min_diff = diff;
                    bestKey = vK;
                }
            }
        }
        System.out.println(String.format("best key: key: %s \t min_diff: %f", bestKey.getKey(), min_diff));
        return bestKey;
    }

}
