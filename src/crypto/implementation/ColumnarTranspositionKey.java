package crypto.implementation;

import crypto.Key;
import helpers.Permutations;

public class ColumnarTranspositionKey implements Key {

    private Integer[] key;
    private Integer[] key_inverse;

    public ColumnarTranspositionKey(Integer[] key_perm){
        this.key = key_perm;
        this.key_inverse = Permutations.inversePermutation(this.key);
    }

    public Integer[] getKey() {
        return key;
    }

    public Integer[] getKey_inverse() {
        return key_inverse;
    }
}
