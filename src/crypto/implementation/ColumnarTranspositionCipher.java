package crypto.implementation;

import crypto.Cipher;
import crypto.Key;

public class ColumnarTranspositionCipher implements Cipher {

    public ColumnarTranspositionCipher(){};

    @Override
    public String encryption(Key key, String text) {
        ColumnarTranspositionKey ctK = (ColumnarTranspositionKey) key;
        // calc size of table
        int y = ctK.getKey().length;
        int x = (int)Math.ceil((double)text.length() / y);
        Character[][] table = new Character[x][y]; // table is redundant
        char[] encrypted_text = new char[x*y];
        for(int i=0; i<x; i++){
            for(int j=0; j<y; j++){
                int col = ctK.getKey()[j] - 1;

                int ix = i*y + j;
                try {
                    table[i][col] = text.charAt(ix); // fill the columns in order based on key
                }catch (StringIndexOutOfBoundsException e){
                    table[i][col] = 'x';
                }

                encrypted_text[col*x + i] = table[i][col];
            }
        }

        //System.out.println("encrypted table");
        //printTable(table, x, y);
        return new String(encrypted_text);
    }

    @Override
    public String decryption(Key key, String text) {
        ColumnarTranspositionKey ctK = (ColumnarTranspositionKey) key;
        int y = ctK.getKey().length;
        int x = text.length() / y;
        Character[][] table = new Character[x][y]; // table is redundant
        char[] decrypted_text = new char[x*y];
        for(int i=0; i<x; i++){
            for(int j=0; j<y; j++) {
                int col = ctK.getKey_inverse()[j] - 1;
                table[i][col] = text.charAt(j*x + i);
                decrypted_text[i*y + col] = table[i][col];
            }
        }

        //System.out.println("decrypted table");
        //printTable(table, x, y);
        return new String(decrypted_text);
    }

    public void printTable(Character[][] table, int x, int y){
        // function that prints 2D table, use for debuging...
        for(int i=0; i<x; i++){
            for(int j=0; j<y; j++){
                System.out.print(table[i][j] + " ");
            }
            System.out.println("");
        }
        System.out.println("");
    }
}
