package crypto.implementation;

import crypto.Cipher;
import crypto.Key;
import helpers.Permutations;

public class SubstitutionKey implements Key{

    private Character[] key; // key is the n-th permutaion
    private Character[] key_inverse;

    public SubstitutionKey(Character[] key) {
            this.key = key;
            this.key_inverse = Permutations.inversePermutation(key);
    }

    /*
    public SubstitutionKey(int n){
        //doesn't work, because factorial(26) is very large, overflow, need manually enter a key
        int offset = Mmath.factorial(26);
        this.key = indexPerm2key(n + offset);
    }
    */

    public Character[] getKey() {
        return key;
    }

    public Character[] getKey_inverse() {
        return key_inverse;
    }

    public String key2String(Character[] key){
        String buff = "";
        for(char x: key){
            buff += x + " ";
        }
        return buff;
    }

    private Integer[] key2perm(Character[] charKey){
        // converts key representation of Character into permutation (Integer representation) 'a' -> 1, 'b' -> 2, ..., 'z' -> 26
        Integer[] intKey = new Integer[26];
        for(int i=0; i<26; i++){
            intKey[i] = (int)(charKey[i] - 'a') + 1;
        }
        return intKey;
    }

    private Character[] perm2key(Integer[] permKey){
        Character[] charKey = new Character[26];
        for(int i=0; i<26; i++){
            char c = (char)(permKey[i] + 'a' - 1);
            charKey[i] = c;
        }
        return charKey;
    }

    private Character[] indexPerm2key(int n){
        // from index_of_permutation (n-th perm) return Character key
        Integer[] perm = Permutations.get_nth_permutation(n);
        return perm2key(perm);
    }
}
