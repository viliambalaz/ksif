package crypto.implementation;

import crypto.Cipher;
import crypto.Key;
import helpers.Matrix;
import helpers.Mmath;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;


public class HillCipher implements Cipher {

    public final String ALPHABET = "abcdefghijklmnopqrstuvwxyz";

    public HillCipher(){}

    @Override
    public String encryption(Key key, String text) {
        HillKey hK = (HillKey)key;
        int sz = hK.getKey().sx;
        int fill = text.length()%sz != 0 ? sz - text.length()%sz : 0; // complement to reminder of modulo
        int len = text.length() + fill;
        Integer[][] itext = new Integer[sz][len/sz];
        char[] encrypted_text = new char[len];

        // align text to matrix size
        for(int i=0; i<fill; i++){
            text += "x";
        }
        for(int i=0, r=0; i<len; i++, r++){
            //System.out.println(String.format("i: %d     r: %d    r%%sz: %d     r/sz: %d", i, r, r%sz, r/sz));
            itext[r%sz][r/sz] = (int)(text.charAt(i) - 'a');
        }
        Matrix m = new Matrix(itext);
        Matrix enc_m = hK.getKey().multitplyMatrix(m).modulo(26);
        //System.out.println(m);
        //System.out.println(enc_m);

        for(int i=0, r=0; i<len; i++, r++){
            //System.out.println(String.format("i: %d     r: %d    r%%sz: %d     r/sz: %d", i, r, r%sz, r/sz));
            int x = enc_m.getMatrix()[r%sz][r/sz] + 'a';
            encrypted_text[i] = (char)x;
        }
        return new String(encrypted_text);
    }

    @Override
    public String decryption(Key key, String text) {
        HillKey hK = (HillKey)key;
        int sz = hK.getKey().sx;
        int fill = text.length()%sz != 0 ? sz - text.length()%sz : 0; // complement to reminder of modulo
        int len = text.length() + fill;
        Integer[][] itext = new Integer[sz][len/sz];
        char[] decrypted_text = new char[len];

        // align text to matrix size
        // be sure to decrypt already alligned text size,
        // otherwise the last characters will be decrypted false
        for(int i=0; i<fill; i++){
            text += "x";
        }
        for(int i=0, r=0; i<len; i++, r++){
            //System.out.println(String.format("i: %d     r: %d    r%%sz: %d     r/sz: %d", i, r, r%sz, r/sz));
            itext[r%sz][r/sz] = (int)(text.charAt(i) - 'a');
        }
        Matrix m = new Matrix(itext);
        Matrix dec_m = hK.getInverse_key().multitplyMatrix(m).modulo(26);
        //System.out.println(m);
        //System.out.println(dec_m);

        for(int i=0, r=0; i<len; i++, r++){
            //System.out.println(String.format("i: %d     r: %d    r%%sz: %d     r/sz: %d", i, r, r%sz, r/sz));
            int x = dec_m.getMatrix()[r%sz][r/sz] + 'a';
            decrypted_text[i] = (char)x;
        }
        return new String(decrypted_text);
    }

    public String encryptionSubstitution(Key hK, Key sK, String text){
        // 1. substitution encryption
        // 2. hill encryption
        SubstitutionCipher sC = new SubstitutionCipher();
        String encrypted_text = sC.encryption(sK, text);
        //System.out.println("tmp subst cipher: " + encrypted_text);
        encrypted_text = encryption(hK, encrypted_text);
        return encrypted_text;
    }

    public String decryptionSubstitution(Key hK, Key sK, String text){
        // reverse order
        SubstitutionCipher sC = new SubstitutionCipher();
        String decrypted_text = decryption(hK, text);
        //System.out.println("tmp subst decrypt: " + decrypted_text);
        decrypted_text = sC.decryption(sK, decrypted_text);
        return decrypted_text;

    }

    /**
     * Hill Cipher cracker by given pair openText and encrypted Text
     * @param openText
     * @param encryptedText
     * @param size - size of key
     * @return all possible keys to pair OT -> ET
     */
    public ArrayList<Matrix> keySolver(String openText, String encryptedText, int size){
        String ot = openText.substring(0, size*size); // work only with one pair
        String et = encryptedText.substring(0, size*size);

        Integer[][] tmp_o = new Integer[size][size];
        Integer[][] tmp_e = new Integer[size][size];
        for(int i=0; i<size; i++){
            for(int j=0; j<size; j++){
                tmp_o[i][j] = ot.charAt(size*i + j) - 'a';
                tmp_e[i][j] = et.charAt(size*i + j) - 'a';
            }
        }
        Matrix om = new Matrix(tmp_o);
        Matrix em = new Matrix(tmp_e);
        //System.out.println(om);
        //System.out.println(em);

        HashMap<String, HashSet<Integer>> possibleKeysValues = new HashMap<>();
        ArrayList<ArrayList<Integer>> combos = Mmath.combinationsReplacement(25, size);
        for(int k=0; k<size*size; k++){
            possibleKeysValues.put(String.format("k%d", k+1), new HashSet<>());
        }
        /*
        example :
        MOST -> KOZA
        {12, 14; 18, 19} -> {10, 14; 25, 0}  Key: {11, 3; 21, 4}
        om.getElem(0, 0), om.getElem(0, 1) == em.getElem(0,0); // 12k1 + 14k3 = 10
        om.getElem(0, 0), om.getElem(0, 1) == em.getElem(0,1) // 12k2 + 14k4 = 14
        om.getElem(1, 0), om.getElem(1, 1) == em.getElem(1,0) // 18k1 + 19k3 = 25
        om.getElem(1, 0), om.getElem(1, 1) == em.getElem(1,1) // 18k2 + 19k4 = 0
        */
        for(ArrayList<Integer> combi: combos){
            // if matrix is 2x2
            /*
            int e1 = (om.getElem(0, 0) * combi.get(0) + om.getElem(0, 1) * combi.get(1)) % 26;
            int e3 = (om.getElem(1, 0) * combi.get(0) + om.getElem(1, 1) * combi.get(1)) % 26;

            if(e1 == em.getElem(0, 0) && e3 == em.getElem(1, 0)){
                //System.out.println("k1, k3: " + combi);
                possibleKeysValues.get("k1").add(combi.get(0));
                possibleKeysValues.get("k3").add(combi.get(1));
            }
            if(e1 == em.getElem(0, 1) && e3 == em.getElem(1, 1)){
                //System.out.println("k2, k4: " + combi);
                possibleKeysValues.get("k2").add(combi.get(0));
                possibleKeysValues.get("k4").add(combi.get(1));
            }
            */

            int[] ex = new int[size];
            for(int i=0; i<size; i++){
                int e = 0;
                for(int j=0; j<size; j++){
                    e += om.getElem(i, j) * combi.get(j);
                }
                ex[i] = e % 26;
            }

            for(int i=0; i<size; i++){
                boolean all = true;
                for(int j=0; j<size; j++){
                    if(ex[j] != em.getElem(j, i)){
                        all = false;
                    }
                }
                if(all){
                    for(int k=i, c=0; k<size*size; k+=size, c++) {
                        String key = String.format("k%d", k+1);
                        possibleKeysValues.get(key).add(combi.get(c));
                    }
                }
            }

        }
        ArrayList<Matrix> possibleKeys = new ArrayList<>();
        ArrayList<ArrayList<Integer>> keyArrays= Mmath.cartesianProduct(new ArrayList<>(possibleKeysValues.values()));
        //System.out.println(possibleKeysValues);
        //System.out.println(keyArrays);
        System.out.println("--------------");
        for(ArrayList<Integer> arr: keyArrays){
            Integer[][] m = new Integer[size][size];
            for (int i=0; i<size; i++){
                for(int j=0; j<size; j++){
                    m[j][i] = arr.get(arr.size() - (i*size + j) -1); // it's in reverse order // transpose ???
                }
            }
            Matrix mx = new Matrix(m);
            //System.out.println(mx);
            try{
                mx.inverseMod(26);
                System.out.println("we found possible key.");
                System.out.println(mx);
                possibleKeys.add(mx);
            } catch (IllegalArgumentException ex){
                //System.out.println("Doesn't exist inverse matrix");
            }
        }
        return possibleKeys;
    }

    public Matrix keyIntersection(ArrayList<ArrayList<Matrix>> allKeys){
        ArrayList<Matrix> possibleKeys = allKeys.get(0);
        for(ArrayList<Matrix> arr: allKeys){
            possibleKeys = Mmath.intersection(possibleKeys, arr);
        }
        if(possibleKeys.size() == 0){
            return null;
        }
        else if (possibleKeys.size() > 1){
            System.out.println("Warning: There is more than one key.");
        }
        return possibleKeys.get(0);
    }
}