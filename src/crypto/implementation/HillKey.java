package crypto.implementation;

import crypto.Key;
import helpers.Matrix;

public class HillKey implements Key{
    private Matrix key;
    private Matrix inverse_key;

    /**
     *
     * @param m - matrix stands for encryption key
     * @throws IllegalArgumentException - if doesn't exist inverse matrix in Z-26 of m
     */
    public HillKey(Matrix m){
        this.key = m;
        try {
            this.inverse_key = m.inverseMod(26);
        } catch (IllegalArgumentException ex){
            throw new IllegalArgumentException("Invalid key for HillCipher. Doesn't exist inverse key.");
        }
    }

    public Matrix getKey() {
        return key;
    }

    public Matrix getInverse_key() {
        return inverse_key;
    }
}
