package crypto.implementation;

import crypto.Cipher;
import crypto.Key;

public class VigenerCipher implements Cipher {

    private String alphabet;
    private int no_char;

    public VigenerCipher(){
        alphabet = Key.ALPHABET;
        no_char = alphabet.length();
    }

    @Override
    public String encryption(Key key, String text) {
        VigenerKey vK = (VigenerKey)key;
        int key_length = vK.getKey().length();
        char[] encrypted_text = new char[text.length()];
        for(int i=0; i<text.length(); i++){
            char a = text.charAt(i);
            char c = vK.getKey().charAt(i % key_length);
            int x = (int)text.charAt(i) + (int)vK.getKey().charAt(i % key_length) - (int)'a';
            encrypted_text[i] = (char) ( ( ((int)text.charAt(i) + (int)vK.getKey().charAt(i % key_length) - 2*(int)'a') % no_char) + (int)'a');
        }
        return new String(encrypted_text);
    }

    @Override
    public String decryption(Key key, String text) {
        VigenerKey vK = (VigenerKey)key;
        int key_length = vK.getKey().length();
        char[] decrypted_text = new char[text.length()];
        for(int i=0; i<decrypted_text.length; i++){
            int x =  ( ((int)text.charAt(i) - (int)vK.getKey().charAt(i % key_length) - 2*(int)'a' + 2*no_char) % no_char);
            decrypted_text[i] = (char)( ( ((int)text.charAt(i) - (int)vK.getKey().charAt(i % key_length) + no_char) % no_char) + (int)'a');
        }
        return new String(decrypted_text);
    }
}
