package crypto;

public interface Cipher {

    public enum CipherType {
        Substitution,
        ColumnarTransposition,
        Vigener,
        Unknown
    }

    public String encryption(Key key, String text);

    public String decryption(Key key, String text);

    public static int getArrayIndex(Object[] arr, Object value) {
        /* return index of first occurence value in arr, -1 if not found */
        int k = -1;
        for(int i=0; i<arr.length; i++){
            if(arr[i].equals(value)){
                k = i;
                break;
            }
        }
        return k;
    }

    public static String repr(Object[] input){
        /* String representation of Object array */
        String buff = "";
        for(Object x: input){
            buff += x + " ";
        }
        return buff;
    }



}
