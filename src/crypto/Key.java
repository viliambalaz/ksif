package crypto;

import java.util.Arrays;

public interface Key {
    // lower-case alphabet without space
    public final String ALPHABET = "abcdefghijklmnopqrstuvwxyz";

    /**
     * @param keyword: keyword for Cipher, should contains only lowercase telegraph symbols
     * @return perm: keyword in permutation representation
     * alphabetically and left to right order
     * HALALI -> [3, 1, 5, 2, 6, 4]
     */
    public static Integer[] word2perm(String keyword){
        Integer[] perm = new Integer[keyword.length()];
        Arrays.fill(perm, 0);
        char[] akeyword = keyword.toCharArray();
        Arrays.sort(akeyword);
        for(int i=0; i<akeyword.length; i++){
            for(int j=0; j<akeyword.length; j++){
                if(keyword.charAt(j) == akeyword[i] && perm[j] == 0){
                    perm[j] = i + 1;
                    break;
                }
            }
        }
        return perm;
    }
}
