package helpers;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Mmath {

    public static boolean isPrime(int input){

        if(input == 1){return false;}
        else if(input == 2){return true;}

        int n = (int)Math.ceil(Math.sqrt(input));
        for(int i=2; i<=n; i++){
            if(input % i == 0){
                return false;
            }
        }
        return true;
    }

    public static int factorial(int n){
        if(n < 0){
            // throw ValueError
            // System.err.println("factorial error {}".format(n)")
            return 0;
        }
        return n==0 ? 1 : n * factorial(n -1);
    }

    public static int nearest_factorial(int n){
        // return nearest smaller( or equal) factorial of given number n
        int k = 0;
        while(Mmath.factorial(k) <= n){
            k++;
        }
        return k;
    }

    public static int determinant(int[][] matrix) throws RuntimeException {
        //functiom return determinant of matrix
        int size = matrix.length;
        if (matrix.length != matrix[0].length)
            throw new RuntimeException();
        if (matrix.length == 2) {
            return matrix[0][0] * matrix[1][1] - matrix[1][0] * matrix[0][1];
        } else {
            int sub_det = 0;
            for (int k = 0; k < size; k++) {
                int[][] sub_matrix = new int[size - 1][size - 1];
                for (int i = 1; i < size; i++) {
                    for (int j = 0, y = 0; j < size; j++) {
                        if (j == k)
                            continue;
                        sub_matrix[i - 1][y] = matrix[i][j];
                        y++;
                    }
                }
                sub_det += Math.pow(-1, k) * matrix[0][k] * determinant(sub_matrix);
            }
            return sub_det;
        }
    }

    /**
     * calc inverse number to x in field Z mod
     * @param x Integer
     * @param mod positive Integer
     * @throws IllegalArgumentException if mod is <= 0
     * @return x^-1 (mod), 0 if doesn't exist
     */
    public static int inverseNumberMod(int x, int mod){
        if (mod <= 0){
            throw new IllegalArgumentException("Mod must be non-negative number.");
        }
        x = Math.floorMod(x, mod);
        for(int i=1; i<mod; i++){
            if (Math.floorMod(x*i, mod) == 1){
                return i;
            }
        }
        return 0;
    }

    /**
     *
     * @param n numbers 0 - n
     * @param r size
     * @return list with all combinations with replacement
     */
    public static ArrayList<ArrayList<Integer>> combinationsReplacement(int n, int r)
    {
        ArrayList<ArrayList<Integer>> combos = new ArrayList<>();
        int nn = n + 1;
        int p = 1;
        for (int i=0; i<r; i++)
            p *= nn;
        for (int i=0; i<p; i++){
            int t = i;
            ArrayList<Integer> combi = new ArrayList<Integer>();
            for (int j=0; j<r; j++){
                combi.add(t % nn);
                t = t / nn;
            }
            combos.add(combi);
        }
        return combos;
    }

    /**
     * get all combination with replacements of length <2, lengthOfList>
     * @param elements
     * @param lengthOfList
     * @return
     */
    public static String[] combinationReplacements(String[] elements, int lengthOfList)
    {
        //initialize our returned list with the number of elements calculated above
        String[] allLists = new String[(int)Math.pow(elements.length, lengthOfList)];

        //lists of length 1 are just the original elements
        if(lengthOfList == 1) return elements;
        else
        {
            //the recursion--get all lists of length 3, length 2, all the way up to 1
            String[] allSublists = combinationReplacements(elements, lengthOfList - 1);

            //append the sublists to each element
            int arrayIndex = 0;

            for(int i = 0; i < elements.length; i++)
            {
                for(int j = 0; j < allSublists.length; j++)
                {
                    //add the newly appended combination to the list
                    allLists[arrayIndex] = elements[i] + allSublists[j];
                    arrayIndex++;
                }
            }

            return allLists;
        }
    }

    public static ArrayList<ArrayList<Integer>> cartesianProduct(ArrayList<HashSet<Integer>> sets) {
        if (sets.size() < 2)
            throw new IllegalArgumentException(
                    "Can't have a product of fewer than two sets (got " +
                            sets.size() + ")");
        return _cartesianProduct(0, sets);
    }

    private static ArrayList<ArrayList<Integer>> _cartesianProduct(int index, ArrayList<HashSet<Integer>> sets) {
        ArrayList<ArrayList<Integer>> ret = new ArrayList<ArrayList<Integer>>();
        if (index == sets.size()) {
            ret.add(new ArrayList<Integer>());
        } else {
            for (Integer obj : sets.get(index)) {
                for (ArrayList<Integer> set : _cartesianProduct(index+1, sets)) {
                    set.add(obj);
                    ret.add(set);
                }
            }
        }
        return ret;
    }

    public static <T> ArrayList<T> union(ArrayList<T> list1, ArrayList<T> list2) {
        Set<T> set = new HashSet<T>();
        set.addAll(list1);
        set.addAll(list2);
        return new ArrayList<T>(set);
    }

    public static <T> ArrayList<T> intersection(ArrayList<T> list1, ArrayList<T> list2) {
        ArrayList<T> list = new ArrayList<T>();
        for (T t : list1) {
            if(list2.contains(t)) {
                list.add(t);
            }
        }
        return list;
    }

    public static void cartesianProductTest(){
        HashSet<Integer> h1 = new HashSet<>();
        h1.add(1); h1.add(2); h1.add(3);
        HashSet<Integer> h2 = new HashSet<>();
        h2.add(1); h2.add(5); h2.add(6);
        HashSet<Integer> h3 = new HashSet<>();
        h3.add(7); h3.add(8);
        ArrayList<HashSet<Integer>> sets = new ArrayList<>();
        sets.add(h1); sets.add(h2); sets.add(h3);
        System.out.println(Mmath.cartesianProduct(sets));
    }

}
