package helpers;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.text.Normalizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;

public class Text {

    public static File pickFromFileChooser() {
        JFileChooser fc = new JFileChooser();
        fc.setCurrentDirectory(new File("."));
        fc.setDialogTitle("Select the file to open... ");
        fc.setMultiSelectionEnabled(false);
        fc.setFileSelectionMode(JFileChooser.FILES_ONLY);

        int returnVal = fc.showOpenDialog(null);

        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File f = fc.getSelectedFile();
            return f;
        }
        return null;
    }

    public static String readText(File file) {
        StringBuilder sb = new StringBuilder();
        if (file.exists()) {
            BufferedReader br = null;
            try {
                br = new BufferedReader(new FileReader(file));
                String line;
                while ((line = br.readLine()) != null) {
                    sb.append(line).append("\n");
                }
            } catch (Exception ex) {
                sb = null;
                Logger.getLogger(Text.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                try {
                    br.close();
                } catch (IOException ex) {
                    Logger.getLogger(Text.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

        }
        return sb.toString();
    }

    public static boolean writeText(File file, String content) {
        if (file.exists()) {
            BufferedWriter bw = null;
            try {
                bw = new BufferedWriter(new FileWriter(file));
                bw.write(content);
                bw.flush();
                return true;
            } catch (IOException ex) {
                Logger.getLogger(Text.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                try {
                    bw.close();
                } catch (IOException ex) {
                    Logger.getLogger(Text.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return false;
    }

    public static void saveToFile(Object o, String path) {
        try {
            FileOutputStream fos = new FileOutputStream(path);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(o);
            oos.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Text.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Text.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static Object readFromFile(String path) {
        try {
            FileInputStream fis = new FileInputStream(path);
            ObjectInputStream ois = new ObjectInputStream(fis);
            Object o = ois.readObject();
            return o;
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Text.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException | ClassNotFoundException ex) {
            Logger.getLogger(Text.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static String convertToTSA(String in, boolean keepSpace){
        // transport random text into telegraph alphabet TSA
        // also remove punctuation
        String regex = keepSpace ? "[^\\p{ASCII}]" : "[[^\\p{ASCII}]\\s]";
        in = in.toLowerCase();
        in = Normalizer.normalize(in, Normalizer.Form.NFD).replaceAll(regex, "");
        return in.replaceAll("[^a-z ]", ""); // remove numbers, colons, symbols, ...
    }

    public static String stringSlice(String str, int start, int end, int step){
        start = start >= 0 ? start : 0;
        end = end < str.length() ? end : str.length();
        step = step > 0 ? step : 1;

        String sliced = new String();
        for(int i=start; i<end; i+=step){
            sliced += str.charAt(i);
        }
        return sliced;
    }

}