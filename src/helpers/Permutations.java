package helpers;

import java.util.*;

public class Permutations {

    /* useful function for working with arrays */
    public static void print_ObjArr(Object[] input){
        /* prints out Object array */
        for(Object x: input){
            System.out.print(x + " ");
        }
        System.out.println("");
    }

    public static int getArrayIndex(Object[] arr, Object value) {
        /* return index of first occurence value in arr, -1 if not found */
        int k = -1;
        for(int i=0; i<arr.length; i++){
            if(arr[i].equals(value)){
                k = i;
                break;
            }
        }
        return k;
    }

    public static void swap_Obj(Object[] input, int i, int j){
        Object temp = input[i];
        input[i] = input[j];
        input[j] = temp;
    }

    public static int[] toInt(HashSet<Integer> set) {
        /* convert Hashset<Integer> to int[] */
        int[] a = new int[set.size()];
        int i = 0;
        for (Integer val : set) a[i++] = val;
        return a;
    }

    /* permutation functions */
    public static void rndPerm(Object[] input){
        /* randomly shuffle elements in permutation by method Knuth */
        int n = input.length;
        for (int i= 0; i <= n-2; i++) {
            int j = i+uniform(n-i); // A random integer such that i ≤ j < n
            // Swap the randomly picked element with permutation[i]
            Object temp = input[i];
            input[i] = input[j];
            input[j] = temp;
        }
    }

    public static void initialize_and_permute(int[] permutation) {
        /* randomly shuffle elements in permutation by method Knuth */
        int n = permutation.length;
        for (int i= 0; i <= n-2; i++) {
            int j = i+uniform(n-i); /* A random integer such that i ≤ j < n  */
            // swap(permutation[i], permutation[j]);   /* Swap the randomly picked element with permutation[i] */

            int temp = permutation[i];
            permutation[i] = permutation[j];
            permutation[j] = temp;
        }
    }

    public static int uniform(int m) {
        /* Returns a random integer 0 <= uniform(m) <= m-1 with uniform distribution */
        Random x = new Random();
        int n = x.nextInt(m); // <0, m-1>
        return n;
    }

    public static Integer[] inversePermutation(Integer[] input){
        /* return inverse permutation of input, permutation must begin with 1 and is continuous
         *input is not being changed
         */
        Integer[] inverse_perm = new Integer[input.length];
        for(int i=0; i<input.length; i++){
            inverse_perm[i] = getArrayIndex(input, i+1) +1;
            // inv[perm[i]] = i;
        }
        //print_ObjArr(inverse_perm);
        return inverse_perm;
    }

    public static Character[] inversePermutation(Character[] input){
        /* inverse permutation for Characters, make sure that input begins with 'a' and is continuous
         * mapping permutation to integers, do inverse permutation, reverse mapping back to Characters
         * input is not being changed
         */
        Character[] inverse_perm = new Character[input.length];
        Integer[] map_int = new Integer[input.length];
        for(int i=0; i<input.length; i++){
            map_int[i] = input[i] - 'a' + 1; // mapping 'a' to 1
        }
        map_int = inversePermutation(map_int);
        for(int i=0; i<input.length; i++){
            char c = (char)(map_int[i] + 'a' - 1);  // temp variables for casting int to char
            inverse_perm[i] = c;
        }
        return inverse_perm;

    }

    public static void allPerm(Object[] input, int l, int r){
        // prints out all permutations
        if(l == r){
            print_ObjArr(input);
        }
        else{
            for(int i=l; i<r+1; i++){
                Object temp = input[i];
                input[i] = input[l];
                input[l] = temp;
                allPerm(input, l+1, r);

                temp = input[i]; // backtrack
                input[i] = input[l];
                input[l] = temp;
            }
        }
    }

    public static int permutation_index(Integer[] input){
        /* return serial number of permutation (n-th permutation)
         * */

        //init mask to {1, 2, ..., n}
        ArrayList<Integer> mask = new ArrayList<Integer>();
        for(int i=0; i<input.length; i++){
            mask.add(i+1);
        }

        int k = 1, f = input.length -1, t;
        // sum_{j=1}^{n} (t_j-1)(n-j)!, t_j <= n-j+1, t - how many numbers are smaller <
        for(int i=0; i<input.length - 1 ; i++){
            t = mask.indexOf(input[i]);
            k += Mmath.factorial(f) * t;
            /*
            String msg = String.format("%d! * %d", f, t);
            System.out.println(msg);
            */
            f--;
            mask.remove(input[i]);
        }
        return k;
    }

    public static int permutation_index(Object input[]){
        /* return serial number of permutation (n-th permutation) of Object type
         *  */
        List mask = new ArrayList<>(Arrays.asList(input));
        Collections.sort(mask);

        int ti[] = new int[input.length];
        for (int j = 0; j < input.length; j++) {
            int ix = mask.indexOf(input[j]);
            ti[j] = ix;
            mask.remove(ix); // order of next element - only remaining elements
        }

        int n = 1;
        for (int j = 0, k = ti.length - 1; j < ti.length - 1; j++, k--) {
            n += ti[j] * Mmath.factorial(k);
        }
        return n;
    }

    public static Integer[] get_nth_permutation(int k){
        /* return permutation with series number k (k - index of permutation)
         * return permutation [Integer] with minimal elements needed to reach index
         */
        ArrayList<Integer> factorial_representation = new ArrayList<Integer>();
        int i = 1;
        int quotient = k - 1;  // substract by one !!
        while(quotient != 0){

            int reminder = quotient % i;
            //String s = String.format("%d // %d quotient: %d  reminder: %d", quotient, i, quotient/i, reminder);
            //System.out.println(s);

            factorial_representation.add(0, reminder);
            quotient = quotient / i;
            i++;
        }

        // convert to array
        Integer[] arr = new Integer[factorial_representation.size()];
        ArrayList<Integer> mask = new ArrayList<>();
        for(int j=0; j < factorial_representation.size(); j++){
            mask.add(j+1);
        }

        for(int j=0; j < factorial_representation.size(); j++){
            int x = factorial_representation.get(j);
            arr[j] = mask.get(x);
            mask.remove(x);
        }
        return arr;
    }

    public static Object[] get_nth_permutation(int n, Object []input) {
        /* return permutation with series number k (k - index of permutation)
         * return permutation [Object]
         * minimal elements needed to reach index
         */
        List<Integer> reminders = new ArrayList<>();
        int i = 1;
        reminders.add(n%i);
        n = n / i;

        while(n > 0){
            i++;
            reminders.add(n % i);
            n = n / i;
        }
        Collections.reverse(reminders); // going from last reminder
        //System.out.println("reminders: "+ reminders.toString());

        List elements = new ArrayList<>(Arrays.asList(input.clone()));
        Collections.sort(elements);
        Object nth_permutation[]= new Object[input.length];
        for(int c=0; c < reminders.size(); c++){
            int ai = reminders.get(c);
            Object el = elements.get(ai);
            elements.remove(el);
            nth_permutation[c] = el;
        }
        return nth_permutation;
    }

    public static ArrayList<String> permutation_cycles(ArrayList<Integer> perm){
        /* split perm with ',' into cycles, stored in String
         * return Array of String with all cycles in perm
         */
        ArrayList<String> cycles = new ArrayList<>();
        ArrayList<Integer> mask = new ArrayList<>();
        for(int j=0; j < perm.size(); j++){
            mask.add(j+1);
        }

        while(mask.size() != 0){
            int i = mask.remove(0);
            String cy = new String();
            HashSet<Integer> current_cycle = new HashSet<>();
            while(current_cycle.add(i)){
                i = perm.get(i - 1);
                cy += i + ",";
            }
            for(int x: current_cycle){
                mask.remove(new Integer(x)); // remove number, not index
            }
            cycles.add(cy);
        }
        return cycles;
    }

    public int cycle_level(String cycle){
        /* return level of permutation cycle
            #param: cycle - from function permutation_cycles
         */
        return cycle.length() - cycle.replace(",", "").length();
    }

    public void mmain(){
        /* testing function, Permutation part1 */
        HashSet<Integer> range = new HashSet<>();
        for(int i=1; i<=10; i++){
            range.add(i);
        }
        int[] arr = toInt(range);

        for(int x: arr){
            System.out.print(x + " ");
        }
        System.out.println("----------");


        for(int i=0; i<20; i++){
            initialize_and_permute(arr);
            for(int x: arr){
                System.out.print(x + " ");
            }
            System.out.println("");
        }

        System.out.println("-------rnd perm");
        Object[] arr_obj = new Object[10];
        for (int i=1; i<=10; i++){
            arr_obj[i-1] = new Integer(i);
        }

        for(int i=0; i<20; i++){
            rndPerm(arr_obj);
            for(Object x: arr_obj){
                System.out.print(x + " ");
            }
            System.out.println("");
        }
        Integer[] arr_int = new Integer[5];
        for (int i=1; i<=5; i++){
            arr_int[i-1] = new Integer(i);
        }
        System.out.println("--------allPerm()--------");
        allPerm(arr_int, 0, 5-1);

        System.out.println("-------inverse_perm------");
        rndPerm(arr_int);
        System.out.println("random perm: ");
        print_ObjArr(arr_int);

        Object[] inverse_perm = inversePermutation(arr_int);
        System.out.println("\ninverse perm: ");
        print_ObjArr(inverse_perm);


        System.out.println("-------inverse_perm-characters-----");
        Character[] arr_obj_char = {'b', 'd', 'e', 'a', 'c'};
        System.out.println("random perm: ");
        print_ObjArr(arr_obj_char);

        Character[] inverse_perm_char = inversePermutation(arr_obj_char);
        System.out.println("inverse perm: ");
        print_ObjArr(inverse_perm_char);

    }

    public void mmain2(){
        /* test function for exercise Permutation part2 */
        System.out.println("----permutation-index-------");
        Integer[] arr = {5, 3, 4, 2, 1, 6};
        int k = permutation_index(arr);
        print_ObjArr(arr);
        System.out.println("nth_permutaion: " + k);

        System.out.println("\n----n_th-permutation-minimal------");
        System.out.println("n_th permutaion of 3796: ");
        k = 3796;
        print_ObjArr(get_nth_permutation(k));

        System.out.println("\n----n_th-permutation-------");
        System.out.println("n_th permutation 3796 by 12 elements: ");
        Integer[] perm12 = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12};
        print_ObjArr(get_nth_permutation(k, perm12));

        System.out.println("\n----n_th-permutation-------");
        System.out.println("n_th permutation 3796 by 12 Character elements");
        Character[] char_perm12 = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l'};
        print_ObjArr(get_nth_permutation(k, char_perm12));

        System.out.println("\n----permutation---index");
        System.out.println("permutation index of {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12}");
        System.out.println("int: " + permutation_index(perm12));
        System.out.println("char " + permutation_index(char_perm12));

        System.out.println("\n-----cycles-----");
        Integer[] arr2 = {6, 2, 5, 1, 4, 7, 3};
        ArrayList<Integer> arrlist = new ArrayList<>();
        Collections.addAll(arrlist, arr2);

        print_ObjArr(arr2);
        for(String x : permutation_cycles(arrlist)){
            System.out.println(x);
        }

        System.out.println("\n----n_th-permutation-minimal------");

        k = Mmath.factorial(10);
        System.out.println("n_th permutaion of: " + k);
        print_ObjArr(get_nth_permutation(k));

    }

    public static Integer[] compoundPermutation(Integer[] perm1, Integer[] perm2){
        assert perm1.length != perm2.length;
        int n = perm1.length;
        Integer[] compound_perm = new Integer[n];
        for(int i=0; i<n; i++){
            compound_perm[i] = perm1[perm2[i] - 1];
        }
        return compound_perm;
    }
}
