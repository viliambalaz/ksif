package helpers;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class TextStatistics {

    //----  debug functions -------------

    // ------------------------------------------

    public static Map<String, Double> readNgram(String txt, int n, boolean relativeFr){
        /**
         * function counts occurrences of n-grams in text
         * @param txt: text
         * @param n: size of anagram
         * @param relativeFr: relative occurrence (in %)
         * @return Map: counter
         */
        Map<String, Double> counter = new HashMap<>();
        int amount = txt.length() - (n-1); // numbers of ngrams
        for(int i=0; i< amount; i++){
            String ngram = txt.substring(i, i+n);
            Double count = counter.containsKey(ngram) ? counter.get(ngram) : new Double(0);
            counter.put(ngram, count + 1);
        }

        if(relativeFr){ // convert into relative occurence
            for(String ngram: counter.keySet()){
                counter.put(ngram, counter.get(ngram)/amount);
            }
        }
        return counter;
    }

    public static String entropy(int n){
        /**
         * generate random text that contains lowercase alphabet [a-z]
         * each character has equal probability
         * (- sum_i p_i * log_2 (p_i)).
         * @param n: number of characters
         * @return String text
         */
        char[] text = new char[n];
        for(int i=0; i<n; i++){
            int x = new Random().nextInt(26);
            text[i] = (char) (x + 'a');
        }
        return new String(text);
    }

    /**
     * we don't normalize index , "don't divide with number of characters"
     * @param p: relative frequency of characters (pairs), not sorted
     * @return Index of Coincidence
     */
    public static Double indexOfCoincidence(Double[] p){

        Double ixc = new Double(0);
        for(Double d: p){
            ixc += d * d;
        }
        return ixc;
    }

    /**
     * @param p: absolute frequency of characters (pairs), not sorted
     * @param N: length of TSA text
     * @return Index of Coincidence
     */
    public static Double indexOfCoincidence(Double[] p, int N){

        Double ixc = new Double(0);
        for(Double d: p){
            ixc += d * (d - 1);
        }
        return ixc / (N * (N - 1)); // divide with number of characters
    }
}
