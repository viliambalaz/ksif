package helpers;

public class Matrix {

    private Integer[][] matrix; // size: [sx * sy]
    public int sx;
    public int sy;

    /**
     *
     * @param matrix - 2D Array of Integers
     * @throws IllegalArgumentException - Matrix don't have same numbers of elements in each row
     */
    public Matrix(Integer[][] matrix) throws IllegalArgumentException{
        for(int i = 0; i< matrix.length-1; i++){
            if(matrix[i].length != matrix[i+1].length){
                throw new IllegalArgumentException("Not the same number of elements in rows.");
            }
        }
        this.matrix = matrix;
        this.sx = this.matrix.length;
        this.sy = this.matrix[0].length;
    }

    /**
     * for debug printing into the console
     * for nice form don't use numbers with len > 5
     * @return matrix in string
     */
    @Override
    public String toString(){
        String s = "{\n";
        for(int i=0; i<this.sx; i++){
            for(int j=0; j<this.sy; j++){
                s += String.format("%5d", this.matrix[i][j]);
            }
            s += "\n";
        }
        s += "}\n";
        return s;
    }

    public Integer[][] getMatrix() {
        return matrix;
    }

    public int getElem(int x, int y){
        return this.matrix[x][y];
    }

    @Override
    public boolean equals(Object obj){
        if(!obj.getClass().isInstance(this)){
            return false;
        }
        Matrix m = (Matrix)obj;
        for(int i=0; i<this.sx; i++){
            for(int j=0; j<this.sy; j++) {
                if(this.matrix[i][j] != m.matrix[i][j]){
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * copy the values into this.matrix
     * @param matrix
     */
    public void setMatrix(Integer[][] matrix) {
        for(int i=0; i<sx; i++){
            for(int j=0; j<sy; j++) {
                this.matrix[i][j] = matrix[i][j];
            }
        }
    }


    /**
     * multiply matrix with constant
     * @param c - constant
     * @return c * this.matrix
     */
    public Matrix multiplyConstant(int c){
        Integer[][] m = new Integer[sx][sy];
        for(int i=0; i<this.sx; i++){
            for(int j=0; j<this.sy; j++) {
                m[i][j] = c * this.matrix[i][j];
            }
        }
        return new Matrix(m);
    }


    /**
     * modulo matrix with constant
     * @param mod - constant
     * @return this.matrix (mod)
     */
    public Matrix modulo(int mod){
        Integer[][] m = new Integer[this.sx][this.sy];
        for(int i=0; i<this.sx; i++){
            for(int j=0; j<this.sy; j++) {
                m[i][j] = Math.floorMod(this.matrix[i][j], mod);
            }
        }
        return new Matrix(m);
    }


    /**
     *
     * @return determinant of matrix
     * @throws IllegalArgumentException - illegal size of matrix, non squared
     */
    public int determinant() throws IllegalArgumentException{
        if(this.matrix.length != this.matrix[0].length){
            throw new IllegalArgumentException("Determinant of non-squared matrix.");
        }
        return determinant(this.matrix);
    }

    private int determinant(Integer[][] matrix)
    {
        // return determinant of matrix
        int size = matrix.length;
        if(matrix.length != matrix[0].length)
            throw new RuntimeException();
        if(matrix.length == 2){
            return matrix[0][0] * matrix[1][1] - matrix[1][0] * matrix[0][1];
        }
        else
        {
            int sub_det = 0;
            for(int k=0; k<size; k++)
            {
                Integer[][] sub_matrix = new Integer[size-1][size-1];
                for(int i=1; i<size; i++)
                {
                    for(int j=0,y=0; j<size; j++)
                    {
                        if(j==k)
                            continue;
                        sub_matrix[i-1][y] = matrix[i][j];
                        y++;
                    }
                }
                /*
                System.out.println(Math.pow(-1, k) *matrix[0][k] + "*");
                for(int[] arr: sub_matrix) {
                    System.out.println(Arrays.toString(arr));
                }
                System.out.println();
                */
                sub_det += Math.pow(-1, k) * matrix[0][k] * determinant(sub_matrix);
            }
            return sub_det;
        }
    }

    /**
     * multiply two matrices
     * @param m - matrix to multiply with
     * @return A*B
     * @throws IllegalArgumentException if matrices dimension don't match aRows != bColumn
     */
    public Matrix multitplyMatrix(Matrix m){
        if(this.sy != m.sx){
            throw new IllegalArgumentException("aColumns: " + this.sy + " did not match bRolumns: " + m.sx + ".");
        }
        Integer[][] result = new Integer[this.sx][m.sy];
        // init new matrix to zeros
        for(int i=0; i<this.sx; i++){
            for(int j=0; j<m.sy; j++){
                result[i][j] = 0;
            }
        }

        for(int i=0; i<this.sx; i++){ //aRows
            for(int j=0; j<m.sy; j++){ // bColumns
                for(int k=0; k<this.sy; k++) { //aColumns
                    result[i][j] += this.matrix[i][k] * m.matrix[k][j];
                }
            }
        }
        return new Matrix(result);

    }

    /**
     *
     * inverse = det()^-1 * adjugate
     * @param mod modulo number in Field Zx
     * @return new Matrix() inverse to original in field Z(mod)
     * @throws IllegalArgumentException  - if matrix is non-squared
     *                                   - if determinant of matrix is 0
     *                                   - if doesn't exist inverse number to determiinant in Z(mod) - field
     */
    public Matrix inverseMod(int mod){
        if(this.sx != this.sy){
            throw new IllegalArgumentException("Inverse of non-squared matrix"); // not necessary, also throws determinant()
        }

        int det = determinant();
        int inv_det = Mmath.inverseNumberMod(det, mod);
        //System.out.println(String.format("det: %d mod: %d inv_det: %d", det, mod, inv_det));
        if(det == 0){
            throw new IllegalArgumentException("Matrix doesn't have inverse matrix. Determinant is 0.");
        } else if(inv_det == 0){
            String errMsg = String.format("Matrix doesn't have inverse matrix in field Z%d. Doesn't exist inverse number to determinant %d.", mod, det);
            throw new IllegalArgumentException(errMsg);
        }

        Matrix result = new Matrix(this.matrix);
        result = result.cofactor();
        result = result.transpose();
        result = result.multiplyConstant(inv_det);
        result = result.modulo(mod);
        return result;
    }

    /**
     * cofactor = (-1)^(i+j)*A(i,j)
     * values in (i,j) are determinants of submatrices(i, j)
     * @return Cofactor of matrix
     */
    public Matrix cofactor(){
        if(this.sx != this.sy){
            throw new IllegalArgumentException("Cofactor of non-squared matrix"); // not necessary,also throws determinant
        }
        Integer[][] result = new Integer[this.sx][this.sy];
        if(this.sx == 2){
            result[0][0] = this.matrix[1][1];
            result[1][1] = this.matrix[0][0];
            result[0][1] = -1 * this.matrix[1][0];
            result[1][0] = -1* this.matrix[0][1];
            return new Matrix(result);
        }
        for(int i=0; i<this.sx; i++){
            for(int j=0; j<this.sy; j++) {
                result[i][j] = (int)Math.pow(-1, i+j) * determinant(this.submatrix(i, j));
            }
        }

        return new Matrix(result);
    }

    /**
     * swap rows with columns
     * @return transpose Matrix
     */
    public Matrix transpose(){
        Integer[][] result = new Integer[this.sx][this.sy];
        for(int i=0; i<this.sx; i++){
            for(int j=0; j<this.sy; j++) {
                result[j][i] = this.matrix[i][j];
            }
        }
        return new Matrix(result);
    }

    /**
     *
     * @param x - row
     * @param y - column
     * @return new Matrix [size -1][size-1] by omitting x-th row and y-th column
     */
    public Integer[][] submatrix(int x, int y){
        Integer[][] m = this.getMatrix();
        Integer result[][] = new Integer[m.length -1][m.length-1]; // must be squared matrix
        int ii = -1, jj; // counters for result matrix
        for(int i=0; i<m.length; i++){
            if(i == x) continue; // miss the x-th row and y-th column
            ii++;
            jj = 0;
            for(int j=0; j<m.length; j++){
                if(j==y) continue;
                result[ii][jj] = m[i][j];
                jj++;
            }
        }
        return result;
    }

}
