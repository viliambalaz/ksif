package helpers;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class Language {

    // #todo: ref for English and German

    public static Double[] ref = {0.08167, 0.01492, 0.02782, 0.04253, 0.12702, 0.02228, 0.02015, 0.06094, 0.06966, 0.00153,
                    0.00772, 0.04025, 0.02406, 0.06749, 0.07507, 0.01929, 0.00095, 0.05987, 0.063269, 0.0905599,
                    0.02758, 0.00978, 0.0236, 0.0015, 0.01974, 0.00074};
    public static double[][] ref_bi = (double[][]) Text.readFromFile("./prilohy/_bigrams.bin"); // 2-gram AJ
    public static Map<String, Double> refMapEnglish_monograms = arr2map(ref);
    public static Map<String, Double> refMapEnglish_bigrams = arr2map(ref_bi);

    static List<String> words =  Node.readDictionaryWords("./prilohy/dictionary_5000.txt");
    public static Node EnglishDictionary = Node.loadDictionary(words);

    public enum LanguageEnum {
        English,
        German,
        Slovak,
        Unknown
    }
    public static double ICEnglish = 0.0665;
    public static double ICGerman = 0.076;
    public static double ICSlovak = 0.0603;

    public static final Map<LanguageEnum, Double> icRef = new HashMap<>();
    static {
        icRef.put(LanguageEnum.English, ICEnglish);
        icRef.put(LanguageEnum.German, ICGerman);
        icRef.put(LanguageEnum.Slovak, ICSlovak);
    }

    public static final Map<LanguageEnum, Map> langRef = new HashMap<>();
    static {
        langRef.put(LanguageEnum.English, refMapEnglish_monograms);
        //langRef.put(LanguageEnum.German, );
        //langRef.put(LanguageEnum.Slovak, );
    }


    public static LanguageEnum guessLanguage(String txt) {
        Collection<Double> values = TextStatistics.readNgram(txt, 1, false).values();
        Double stat[] = new Double[values.size()];
        values.toArray(stat);
        double ic = TextStatistics.indexOfCoincidence(stat, txt.length());

        LanguageEnum bestLang = LanguageEnum.Unknown;
        double minDistance = 1;
        for (Map.Entry<LanguageEnum, Double> entrySet : icRef.entrySet()) {
            Language.LanguageEnum key = entrySet.getKey();
            Double value = entrySet.getValue();
            double dist = java.lang.Math.abs(ic - value);
            if ((dist < minDistance) && (dist < 0.01)) { // can't differ to much
                minDistance = dist;
                bestLang = key;
            }
        }
        return bestLang;
    }

    public static void print_ref(double [][]ref){
        System.out.println("rows: " + ref.length);
        System.out.println("cols: " + ref[0].length);
        for(int i=0; i<ref.length; i++){
            for(int j=0; j<ref[i].length; j++){
                String bigram = "" + (char)((char)i + 'a') + (char)((char)j + 'a');
                System.out.println(bigram + ": " + ref[i][j] + " ");
            }
            System.out.println();
        }
    }

    public static Map<String, Double> arr2map(Double[] ref){
        Map<String, Double> refMap = new HashMap<>();
        for(char c='a'; c <= 'z'; c++){
            refMap.put(Character.toString(c), ref[c-'a']);
        }
        return refMap;
    }

    public static Map<String, Double> arr2map(double[][] ref){
        Map<String, Double> refMap = new HashMap<>();
        for(char c='a'; c <= 'z'; c++){
            for(char k='a'; k<='z'; k++){
                refMap.put("" + c + k, ref[c-'a'][k-'a']);
            }
        }
        return refMap;
    }

}
